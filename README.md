# Springboot整合RabbitMq，消息队列搞起来

> https://blog.csdn.net/wayhb/article/details/122450139

- maven项目
- a-rabbitmq-provider 消息生产者
- b-rabbitmq-consumer 消息消费者

## 演示了三种交换机
- 直连交换机
- 群发交换机
- 主题交换机

> 感谢 https://blog.csdn.net/qq_35387940/article/details/100514134