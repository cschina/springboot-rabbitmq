package com.example.arabbitmqprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ARabbitmqProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ARabbitmqProviderApplication.class, args);
    }

}
