package com.example.brabbitmqconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BRabbitmqConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BRabbitmqConsumerApplication.class, args);
    }

}
