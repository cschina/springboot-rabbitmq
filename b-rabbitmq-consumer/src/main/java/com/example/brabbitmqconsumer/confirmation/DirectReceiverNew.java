package com.example.brabbitmqconsumer.confirmation;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;
/**
 * direct exchange 消费者2接受消息
 * 多个消费者实现了轮询的方式对消息进行消费，而且不存在重复消费。
 * 一人消费一个，依次消费，不重复消费
 */
@Component
@RabbitListener(queues = "TestDirectQueue")//监听的队列名称 TestDirectQueue
public class DirectReceiverNew {

    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("@2@---DirectReceiver消费者收到消息  : " + testMessage.toString());
    }

}
