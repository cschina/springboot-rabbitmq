package com.example.brabbitmqconsumer.confirmation;

import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

/**
 * 消息接收处理类
 * 手动确认模式需要实现 ChannelAwareMessageListener
 * 这里的获取消息转换，只作参考，如果报数组越界可以自己根据格式去调整。
 */
@Log4j2
@Component
public class MyAckReceiver implements ChannelAwareMessageListener {

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        //当前消息到的数据的唯一id;
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            //这里可以对消息进行处理后，判断消息是否正确处理或重发
            log.info("消费者收到消息，并手动确认处理：："+message.toString());
            /**
             * channel.basicAck(deliveryTag, true)--用于肯定确认
             * channel.basicNack(deliveryTag, true)--用于否定确认（注意：这是AMQP 0-9-1的RabbitMQ扩展）
             * channel.basicReject(deliveryTag, true)--用于否定确认，但与basic.nack相比有一个限制:一次只能拒绝单条消息
             */
            //deliveryTag:当前参数唯一id,第二个参数，手动确认可以被批处理，当该参数为 true 时，则可以一次性确认 delivery_tag 小于等于传入值的所有消息
            channel.basicAck(deliveryTag, true);
            //第二个参数，true会重新放回队列，所以需要自己根据业务逻辑判断什么时候使用拒绝
//			channel.basicReject(deliveryTag, true);
        } catch (Exception e) {
            channel.basicReject(deliveryTag, false);
            e.printStackTrace();
        }
    }
}